const URL = 'http://localhost:8081/api/auth/register';
 export default async function createUser(newUser){   
    fetch(URL, {
      method: 'post',
      headers: new Headers({
        'Content-Type': 'application/json',
      }),
     body: JSON.stringify(newUser)
    })
     .then(resp => {
       if(!resp.ok) {
         if(resp.status >=400 && resp.status < 500) {
           return resp.json().then(data => {
             let err = {errorMessage: data.message};
             throw err;
           })
         } else {
           let err = {errorMessage: 'Please try again later, server is not responding'};
           throw err;
         }
       }
       return resp.json();
    })
}
