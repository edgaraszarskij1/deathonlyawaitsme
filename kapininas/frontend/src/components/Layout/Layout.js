import React from 'react';
import Aux from '../../hoc/Auxssa';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import DrawLayout from '../../containers/Layout/DrawLayout/LayoutDrawer';
import CssBaseline from '@material-ui/core/CssBaseline/CssBaseline'

const styles = {
    root: {
      display: "flex",
      
    },
  };

function layout(props){
    const { children } = props;
    return(
   <Aux>
     <DrawLayout/>
      <CssBaseline/>
        <div >
        <main>
        {children}
      </main>
      </div>
      </Aux>
    )
}

layout.propTypes = {
    classes: PropTypes.object.isRequired,
  };

export default withStyles(styles)(layout);