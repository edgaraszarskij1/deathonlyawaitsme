import React, { Component } from 'react';
import { Route, Switch } from 'react-router-dom';
import Test from "./containers/test";
import Layout from './components/Layout/Layout';
import ThemeColor from './containers/Layout/LayoutTheme';

class App extends Component {
  render() {
    return (
      <div>
        <ThemeColor>
        <Layout>
      <Switch>
        <Route path = "/" exact component = { Test }/>
      </Switch>
      </Layout>
      </ThemeColor>
      </div>
    );
  }
}

export default App;
