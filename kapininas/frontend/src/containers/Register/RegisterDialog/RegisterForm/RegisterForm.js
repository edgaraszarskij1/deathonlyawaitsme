import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';
import InputLabel from '@material-ui/core/InputLabel';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';

const styles = theme => ({
    container: {
      display: 'flex',
      flexWrap: 'wrap',
    },
    textField: {
      marginLeft: theme.spacing.unit,
      marginRight: theme.spacing.unit,
      width: 250,
    },
    dense: {
      marginTop: 19,
    },
    menu: {
      width: 200,
    },
    formControl: {
      margin: theme.spacing.unit,
      width:68,
      marginTop: 15,  
    },
    selectEmpty: {
      marginTop: theme.spacing.unit * 2,
    },
  });

  function selectYear(){
    var arr = [];
    for (let i = 2019; i >= 1900 ; i--) {
        arr.push(<option key={i} value={i}>{i}</option>)
    }
    return arr; 
  }

  function selectMonth(){
    var arr = [];
  
    for (let i = 1; i <= 12  ; i++) {
      if(i<=9){
        arr.push(<option key={i} value={i}>{'0'+i}</option>)
      }else
        arr.push(<option key={i} value={i}>{i}</option>)
    }
    return arr; 
  }
  function selectDay(){
    var arr = [];
    for (let i = 1; i <= 31 ; i++) {
      if(i<=9){
        arr.push(<option key={i} value={i}>{'0'+i}</option>)
      }else
        arr.push(<option key={i} value={i}>{i}</option>)
    }
    return arr; 
  }

class RegisterForm extends Component {
    constructor(props){
      super(props)
      this.state = {
        email: '',
        firstName: '',
        lastName: '',
        year: '',
        month:'',
        day: '',
        user:{
        password: '',
        repeatPassword: '',
        },
        address: '' ,
      };
    }

     onSubmit1 = () => {
      this.props.onDataGathered({
        email:this.state.email,
        firstName: this.state.firstName,
        lastName: this.state.lastName,
        address: this.state.address,
        password: this.state.user.password,   
      });
      
      this.setState({
        email: '',
        firstName: '',
        lastName: '',
        year: '',
        month:'',
        day: '', 
        user:{
          ...this.state.user,
        password: '',
        repeatPassword: '',
        },
        address: '' ,
      })    
    };

    componentDidMount=()=>{
      this.props.submitionEvent.register(this.onSubmit1);
    }
  
      handleChange = name => event => {
        this.setState({ [name]: event.target.value });
      };

    render(){
        const { classes } = this.props; 
        
        return(
           
            <form className={classes.container} noValidate autoComplete="off" >
        <TextField
          id="standard-name"
          label="Email"
          className={classes.textField}
          value={this.state.email}
          onChange={this.handleChange('email')}
          margin="normal"
        />
        <TextField
          id="firstName"
          label="Vardas"
          className={classes.textField}
          value={this.state.firstName}
          onChange={this.handleChange('firstName')}
          margin="normal"
        />
        <TextField
          id="lastName"
          label="Pavardė"
          className={classes.textField}
          value={this.state.lastName}
          onChange={this.handleChange('lastName')}
          margin="normal"
        />
          <FormControl className={classes.formControl}>
          <InputLabel htmlFor="age-native-simple">Metai</InputLabel>
          <Select
            native
            value={this.state.year}
            onChange={this.handleChange('year')}
            inputProps={{
              name: 'year',
              id: 'age-native-simple',
            }}
          >
            <option value="" />
            {selectYear()}
          </Select>
        </FormControl>
        <FormControl className={classes.formControl}>
          <InputLabel htmlFor="month-native-simple">Menuo</InputLabel>
          <Select
            native
            value={this.state.month}
            onChange={this.handleChange('month')}
            inputProps={{
              name: 'month',
              id: 'month-native-simple',
            }}
          >
            <option value="" />
            {selectMonth()}
          </Select>
        </FormControl>
        <FormControl className={classes.formControl}>
          <InputLabel htmlFor="day-native-simple">Diena</InputLabel>
          <Select
            native
            value={this.state.day}
            onChange={this.handleChange('day')}
            inputProps={{
              name: 'day',
              id: 'day-native-simple',
            }}
          >
            <option value= ""/>
            {selectDay()}
          </Select>
        </FormControl>
        <TextField
          id="address"
          label="Adresas"
          className={classes.textField}
          value={this.state.address}
          onChange={this.handleChange('address')}
          margin="normal"
        />
        {<div style= {{width: '100%'}}/>}
        <TextField
          id="standard-password-input"
          label="Slaptažodis"
          className={classes.textField}
          type="password"
          onChange={this.handleChange('user.password')}
          margin="normal"
        />
         <TextField
          id="standard-password-inputRepeat"
          label="Pakartoti slaptažodį"
          className={classes.textField}
          type="password"
          onChange={this.handleChange('user.repeatPassword')}
          margin="normal"
        />
        </form>
       
        )
    }
}

RegisterForm.propTypes = {
    classes: PropTypes.object.isRequired,
  };

export default withStyles(styles)(RegisterForm);