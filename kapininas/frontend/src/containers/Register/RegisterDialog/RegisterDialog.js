import React, { Component } from 'react';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';
import RegisterForm from './RegisterForm/RegisterForm';
import Event from './RegisterForm/RegisterEvent';

class RegisterDialog extends Component {
  constructor(props){
    super(props)
    this.state = {
      open: true,
      scroll: 'paper',
    };
    this.onDataGathered = this.onDataGathered.bind(this);
  }
 

  handleClickOpen = () => {
    this.setState({ open: true });
  };

  handleClose = () => {
    this.setState({ open: false });
  };

   onDataGathered(data) {
     console.log(data)
      }

  render() {
    const submitEvent = new Event()
    
    return (
      <div>
        <Dialog
          open={this.state.open}
          onClose={this.handleClose}
          scroll={this.state.scroll}
          maxWidth='sm'
        >
          <DialogTitle id="draggable-dialog-title">Registracija</DialogTitle>
          <DialogContent>
              <RegisterForm  submitionEvent={submitEvent} onDataGathered={this.onDataGathered}  />
          </DialogContent>
          <DialogActions>
            <Button onClick={this.handleClose}  color="secondary" >
              Atšaukti
            </Button>
            <Button onClick={() => submitEvent.notify()} color="secondary">
              Registruotis
            </Button>
          </DialogActions>
        </Dialog>
      </div>
    );
  }
}

export default RegisterDialog;