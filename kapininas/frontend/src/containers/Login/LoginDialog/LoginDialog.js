import React, { Component } from 'react';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';
import LoginForm from './LoginForm/LoginForm'


class LoginDialog extends Component {
  state = {
    open: true,
    scroll: 'paper',
  };

  handleClickOpen = () => {
    this.setState({ open: true });
  };

  handleClose = () => {
    this.setState({ open: false });
  };

  render() {
    return (
      <div>
        <Dialog
          open={this.state.open}
          onClose={this.handleClose}
          scroll={this.state.scroll}
          maxWidth='xs'
        >
          <DialogTitle id="draggable-dialog-title">Prisijungimas</DialogTitle>
          <DialogContent>
              <LoginForm/>
          </DialogContent>
          <DialogActions>
            <Button onClick={this.handleClose}  color="secondary" >
              Atšaukti
            </Button>
            <Button onClick={()=>{
              this.handleClose();this.props.onSubmit()
              }}  color="secondary">
              Registruotis
            </Button>
          </DialogActions>
        </Dialog>
      </div>
    );
  }
}

export default LoginDialog;