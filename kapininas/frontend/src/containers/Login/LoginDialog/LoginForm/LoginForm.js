import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';
//import { MuiPickersUtilsProvider, TimePicker, DatePicker } from 'material-ui-pickers';


const styles = theme => ({
    container: {
      display: 'flex',
      flexWrap: 'wrap',
    },
    textField: {
      marginLeft: theme.spacing.unit,
      marginRight: theme.spacing.unit,
      width: 300,
    },
    dense: {
      marginTop: 19,
    },
    menu: {
      width: 200,
    },
  });
  

class RegisterForm extends Component {
    state = {
       email: '',
       password: '',

      };

      handleChange = name => event => {
        this.setState({ [name]: event.target.value });
      };

    render(){
        const { classes } = this.props; 
        return(
           
            <form className={classes.container} noValidate autoComplete="off">
        <TextField
          id="login-email"
          label="Email"
          className={classes.textField}
          value={this.state.email}
          onChange={this.handleChange('email')}
          margin="normal"
          variant="outlined"
        />
        <TextField
          id="login password"
          label="Slaptažodis"
          className={classes.textField}
          type="password"
          autoComplete="current-password"
          margin="normal"
          variant="outlined"
        />
        </form>
       
        )
    }
}

RegisterForm.propTypes = {
    classes: PropTypes.object.isRequired,
  };

export default withStyles(styles)(RegisterForm);