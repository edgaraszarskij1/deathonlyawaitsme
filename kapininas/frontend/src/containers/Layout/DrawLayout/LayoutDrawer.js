import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import IconButton from '@material-ui/core/IconButton';
import MenuIcon from '@material-ui/icons/Menu';
import RegisterDialog from '../../Register/RegisterDialog/RegisterDialog';
import Login from '../../Login/LoginDialog/LoginDialog';
import List from "@material-ui/core/List";
import Divider from "@material-ui/core/Divider";
import ListItem from "@material-ui/core/ListItem";
import ListItemIcon from "@material-ui/core/ListItemIcon";
import ListItemText from "@material-ui/core/ListItemText";
import InboxIcon from "@material-ui/icons/MoveToInbox";
import MailIcon from "@material-ui/icons/Mail";
import SwipeableDrawer from "@material-ui/core/SwipeableDrawer";
import { Link } from "react-router-dom";

const styles = {
  root: {
    flexGrow: 1,
  },
  grow: {
    flexGrow: 1,
  },
  menuButton: {
    marginLeft: -12,
    marginRight: 20,
  },
  list: {
    width: 250
  },
  fullList: {
    width: "auto"
  }
};

class LayoutDrawer extends Component {
  constructor(props){
    super(props)
    this.state = {
      isRegisterOpen:false,
      isLoginOpen:false,
      left: false,
    }
    this.toggleDrawer = this.toggleDrawer.bind(this)
  }


  toggleDrawer = (side, open) => () => {
    this.setState({
      [side]: open
    });
  };

    render(){
        const { classes } = this.props;
        const sideList = (
          <div className={classes.list}>
            <List>
              {["Inbox", "Starred"].map((text, index) => (
                <ListItem button key={text} component = { Link } to="/">
                  <ListItemIcon>
                    {index % 2 === 0 ? <InboxIcon /> : <MailIcon />}
                  </ListItemIcon>
                  <ListItemText primary={text} />
                </ListItem>
              ))}
            </List>
            <Divider />
            <List>
              {["All mail"].map((text, index) => (
                <ListItem button key={text}>
                  <ListItemIcon>
                    {index % 2 === 0 ? <InboxIcon /> : <MailIcon />}
                  </ListItemIcon>
                  <ListItemText primary={text} />
                </ListItem>
              ))}
            </List>
          </div>
        );

    return (
        <div className={classes.root}>
          <AppBar position="fixed">
            <Toolbar>
              <IconButton className={classes.menuButton} color="inherit" aria-label="Menu" onClick={this.toggleDrawer("left", true)}>
                <MenuIcon />
              </IconButton>
              <Typography variant="h6" color="inherit" className={classes.grow}>
                Kapinynas
              </Typography>
              <Button color="inherit" onClick={()=>this.setState(prevState => ({isLoginOpen: !prevState.isLoginOpen}))} >Prisijungti </Button>
              <Button color="inherit" onClick={()=>this.setState(prevState => ({isRegisterOpen: !prevState.isRegisterOpen}))}>Registruotis</Button>
            </Toolbar>
            <SwipeableDrawer
          open={this.state.left}
          onClose={this.toggleDrawer("left", false)}
          onOpen={this.toggleDrawer("left", true)}
        >
          <div
            tabIndex={0}
            role="button"
            onClick={this.toggleDrawer("left", false)}
            onKeyDown={this.toggleDrawer("left", false)}
          >
            {sideList}
          </div>
        </SwipeableDrawer>
          </AppBar>
          {this.state.isRegisterOpen?<RegisterDialog  />:null}
          {this.state.isLoginOpen?<Login onSubmit={()=>{
            console.log('return');
          }}/>:null}
        </div>
      );
    }
  
}

LayoutDrawer.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(LayoutDrawer);