import React, { Component } from 'react'
import {createMuiTheme} from '@material-ui/core/styles';
import MuiThemeProvider from '@material-ui/core/styles/MuiThemeProvider';

const theme = createMuiTheme({
  palette: {
      primary: {
          main: '#bdbdbd',
      },
      secondary: {
          main: '#424242',
      },
  },
  typography: {
    useNextVariants: true,
  }
});


class ThemeColor extends Component {
    render() {
        const { children } = this.props
        return (
            <div >
                <MuiThemeProvider theme={theme}>
                  <main>
                      {children}
                  </main>
                </MuiThemeProvider>
            </div>
        );
    }
}

export default ThemeColor;