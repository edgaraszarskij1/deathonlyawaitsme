const mongoose = require('mongoose');
const User = require('./user');

const petTombSchema = new mongoose.Schema({
    petName:{
        type: String,
        required: true,
        unique: true
    },
    birthDate:{
        type: Date,
        
    },
    deathDate:{
        type: Date,
        
    },
    picture:{
        type: String,
        
    },
    race:{
        type: String,
    },
    gender:{
        type: String,
        
    },
    comment:{
        type: String
    },
    tombType: {
        type: String
    },
    candle: {
        type: Boolean
    },    
    flowers:{
        type: Boolean
    },
    user:{
        type:mongoose.Schema.Types.ObjectId,
        ref: 'User'
    }
},  
 {
    timestamps:true
});

petTombSchema.pre('remove', async function(next) {
    try{
        // finding a user
        let user = await User.findById(this.user);
        //removing like a splice just mongoose method
        user.mongoose.remove(this.id);
        // save 
        await user.save();
        return next();
    }catch(error){
        return next(error);
    }
})

const Pet = mongoose.model('Pet',petTombSchema);

module.exports = Pet;