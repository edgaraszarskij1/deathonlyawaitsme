// connecting to database and setting up mongoose
const mongoose = require('mongoose');
mongoose.set('useCreateIndex', true);
mongoose.set('debug',true);
mongoose.Promise = Promise;
mongoose.connect('mongodb://localhost/Kapininas',{
  keepAlive: true,
  useNewUrlParser: true
});

module.exports.User = require('./user');
module.exports.Pet = require('./petTomb')