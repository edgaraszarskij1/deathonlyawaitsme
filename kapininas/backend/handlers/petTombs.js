const db = require('../models')

exports.createPetTomb = async function(request,response,next){
    try{    
        let petTomb = await db.Pet.create({
            petName:request.body.petName,
            birthDate:request.body.birthDate,
            deathDate:request.body.deathDate,
            picture:request.body.picture,
            race:request.body.race,
            gender:request.body.gender,
            comment:request.body.comment,
            tombType:request.body.tombType,
            user:request.params.id
        });
        let searchUser = await db.User.findById(request.params.id);
        searchUser.pets.push(petTomb.id);
        await searchUser.save()
        let searchPet = await db.Pet.findById(petTomb.id).populate('user',{
            email: true,
            firstName: true,
            lastName: true
        });
        return response.status(200).json(searchPet);
    }catch(error){
        return next(error)
    }
}

exports.getPetTomb = async function(request,response,next){
    try{
        let pet = await db.Pet.find(request.params.pet_id)
        return response.status(200).json(pet)
    }catch(error){
        return next(error);
    }
}

exports.deletePetTomb = async function(request,response,next){
    try{
        let foundPet = await db.Pet.findById(request.params.pet_id)
        await foundPet.remove();
        return response.status(200).json(foundPet);
    }catch(error){
        return next(error);
    }
}