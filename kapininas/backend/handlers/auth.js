const db = require('../models');
const jwt = require('jsonwebtoken');

exports.login = async function(request,response,next){
    //finding user
    try{
        let user = await db.User.findOne({
            email: request.body.email,
        });
        let {id, email, firstName,lastName} = user
        let isMatch = await user.comparePassword(request.body.password);
        if(isMatch){
            let token = jwt.sign({
                id,
                email,
                firstName,
                lastName
            },
            process.env.SECRET_KEY
            );
            return response.status(200).json({
                id,
                email,
                firstName,
                lastName,
                token
            });
        }else{
            return next({
                status:400,
                message:'Invalid Password/Email'
            });
        }
    }catch(err){
        return next({
            status:400,
            message:'Invalid Password/Email'
        });
    }
};

exports.register = async function(request, response, next){
    try{
         let user = await db.User.create(request.body);
         let { id, email,firstName,lastName} = user;
         let token = jwt.sign({
             id:id,
             email:email,
             firstName,
             lastName
         }, process.env.SECRET_KEY)
         return response.status(200).json({
            id:id,
            email:email,
            firstName,
            lastName,
            token
         })

    }catch(err){
        if(err.code === 11000){
            err.message = "User exist";
        }
        return next({
            status: 400,
            message: err.message
        })
    }
};