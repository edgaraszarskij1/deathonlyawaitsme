const express = require('express');
const router = express.Router({mergeParams:true});

const { createPetTomb, getPetTomb, deletePetTomb } = require('../handlers/petTombs');

router.route('/').post(createPetTomb);

router.route('/:pet_id').get(getPetTomb).delete(deletePetTomb);

module.exports = router;