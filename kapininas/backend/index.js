require('dotenv').config();
const express = require("express");
const app = express();
const cors = require("cors");
const bodyParser = require("body-parser");
const errorHandler = require('./handlers/error');
const authRoutes = require('./routes/auth');
const petRoutes = require('./routes/petTombs');
const { loginRequired, ensureCorrectUser } = require('./middleware/auth');
const db = require('./models');

const PORT = 8081;
app.use(cors())
app.use(bodyParser.json());
app.use('/api/auth',authRoutes);
app.use('/api/users/:id/petTombs',
loginRequired,
ensureCorrectUser,
petRoutes
); 

app.get('/api/petTombs', async function(request,response,next){
    try{
        let petTombs = await db.Pet.find().sort({createdAt:'desc'}).populate('user',{
            email:true,
            firstName:true,
            lastName:true
        })
        return response.status(200).json(petTombs)
    }catch(error){
        return next(error);
    }
})

app.use(function(req,res,next){
    let err = new Error("Not found")
    err.status = 404;
    next(err);
});

app.use(errorHandler);

app.listen(PORT, function(){
console.log('veikia');
});