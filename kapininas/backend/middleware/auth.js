require('dotenv').load();
const jwt = require('jsonwebtoken');

exports.loginRequired = function(request,response,next){
    try{
        const token = request.headers.authorization.split(" ")[1];
        jwt.verify(token,process.env.SECRET_KEY,function(error,payload){
            if(payload){
                return next();
            }else{
                return next({
                    status:401,
                    message:"Login first"
                })
            }
        });
    }catch(error){
        return next({
            status:401,
                    message:"Login First"
        })
    }
}

exports.ensureCorrectUser = function(request,respone,next){
    try{
        const token = request.headers.authorization.split(" ")[1];
        jwt.verify(token,process.env.SECRET_KEY,function(error,payload){
            if(payload && payload.id === request.params.id){
                return next();
            }else{
                return next({
                    status: 401,
                    message:"Unauthorized"
                })
            }
        });
    }catch(error){
        return next({
            status: 401,
            message:"Unauthorized"
        })
    }
}